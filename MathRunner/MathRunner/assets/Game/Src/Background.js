
cc.Class({
    extends: cc.Component,

    properties: {
        speed: {
            default: null, //set by onload game.js
        },
        minXPos: {
            default: -960,
            type: 'Integer'
        },
        maxXPos: {
            default: 960,
            type: 'Integer'
        },
        theme: {
            default: 'cp',
        },
        cp1PF: {
            default: null,
            type: cc.Prefab
        },
        cp2PF: {
            default: null,
            type: cc.Prefab
        },
        cp3PF: {
            default: null,
            type: cc.Prefab
        },
        hill1PF: {
            default: null,
            type: cc.Prefab
        },
        hill2PF: {
            default: null,
            type: cc.Prefab
        },
        hill3PF: {
            default: null,
            type: cc.Prefab
        },
        hill4PF: {
            default: null,
            type: cc.Prefab
        },
        hill5PF: {
            default: null,
            type: cc.Prefab
        },
        hill6PF: {
            default: null,
            type: cc.Prefab
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        if (this.theme == 'cp') {
            this.instanciateCP();
        }
        else {
            this.instanciateHill();
        }
    },

    start() {
        this.speedMultiplier = this.layersArray.length > 3 ? 2 : 3;
    },

    update(dt) {
        for (let i = 0; i < this.layersArray.length; i++) {
            let children = this.layersArray[i].children;
            children.forEach(element => {
                element.x -= (this.speed / Math.pow(this.speedMultiplier, this.layersArray.length - i)) * dt;
                if (element.x <= this.minXPos) {
                    element.x = this.maxXPos;
                }
            });
        }
    },


    instanciateCP() {
        this.layersArray = [];
        //Layer 1
        this.layer1 = new cc.Node();
        this.layer1.parent = this.node;
        this.layer1.zIndex = -4;
        let lay1 = cc.instantiate(this.cp1PF);
        lay1.setPosition(0, 0);
        let lay1Copy = cc.instantiate(this.cp1PF);
        lay1Copy.setPosition(960, 0);
        this.layer1.addChild(lay1);
        this.layer1.addChild(lay1Copy);
        this.layersArray.push(this.layer1);

        //layer 2
        this.layer2 = new cc.Node();
        this.layer2.parent = this.node;
        this.layer2.zIndex = -3;
        let lay2 = cc.instantiate(this.cp2PF);
        lay2.setPosition(0, 0);
        let lay2Copy = cc.instantiate(this.cp2PF);
        lay2Copy.setPosition(960, 0);
        this.layer2.addChild(lay2);
        this.layer2.addChild(lay2Copy);
        this.layersArray.push(this.layer2);

        //layer 3
        this.layer3 = new cc.Node();
        this.layer3.parent = this.node;
        this.layer3.zIndex = -2;
        let lay3 = cc.instantiate(this.cp3PF);
        lay3.setPosition(0, 0);
        let lay3Copy = cc.instantiate(this.cp3PF);
        lay3Copy.setPosition(960, 0);
        this.layer3.addChild(lay3);
        this.layer3.addChild(lay3Copy);
        this.layersArray.push(this.layer3);

        // for (let i =0; i<6; i++){
        //     this.layer = new cc.Node();

        // var tempNamespace = {};
        // var myString = "myVarProperty";
        // tempNamespace[myString] = 5;
        //access as tempNamespace.myVarProperty (now 5),
        // }
    },

    instanciateHill() {
        this.layersArray = [];

        this.layer1 = new cc.Node();
        this.layer1.parent = this.node;
        this.layer1.zIndex = -6;
        let lay1 = cc.instantiate(this.hill1PF);
        lay1.setPosition(0, 0);
        let lay1Copy = cc.instantiate(this.hill1PF);
        lay1Copy.setPosition(960, 0);
        this.layer1.addChild(lay1);
        this.layer1.addChild(lay1Copy);

        this.layersArray.push(this.layer1);

        this.layer2 = new cc.Node();
        this.layer2.parent = this.node;
        this.layer2.zIndex = -5;
        let lay2 = cc.instantiate(this.hill2PF);
        lay2.setPosition(0, -225);
        let lay2Copy = cc.instantiate(this.hill2PF);
        lay2Copy.setPosition(960, -225);
        this.layer2.addChild(lay2);
        this.layer2.addChild(lay2Copy);

        this.layersArray.push(this.layer2);

        this.layer3 = new cc.Node();
        this.layer3.parent = this.node;
        this.layer3.zIndex = -4;
        let lay3 = cc.instantiate(this.hill3PF);
        lay3.setPosition(0, -214);
        let lay3Copy = cc.instantiate(this.hill3PF);
        lay3Copy.setPosition(960, -214);
        this.layer3.addChild(lay3);
        this.layer3.addChild(lay3Copy);

        this.layersArray.push(this.layer3);

        this.layer4 = new cc.Node();
        this.layer4.parent = this.node;
        this.layer4.zIndex = -3;
        let lay4 = cc.instantiate(this.hill4PF);
        lay4.setPosition(0, -152);
        let lay4Copy = cc.instantiate(this.hill4PF);
        lay4Copy.setPosition(960, -152);
        this.layer4.addChild(lay4);
        this.layer4.addChild(lay4Copy);

        this.layersArray.push(this.layer4);

        this.layer5 = new cc.Node();
        this.layer5.parent = this.node;
        this.layer5.zIndex = -2;
        let lay5 = cc.instantiate(this.hill5PF);
        lay5.setPosition(0, -301);
        let lay5Copy = cc.instantiate(this.hill5PF);
        lay5Copy.setPosition(960, -301);
        let lay6 = cc.instantiate(this.hill6PF);
        lay6.setPosition(0, -298);
        let lay6Copy = cc.instantiate(this.hill6PF);
        lay6Copy.setPosition(960, -298);
        this.layer5.addChild(lay5);
        this.layer5.addChild(lay5Copy);
        this.layer5.addChild(lay6);
        this.layer5.addChild(lay6Copy);

        this.layersArray.push(this.layer5);
    }
});
/**
 * Controlls the movement of the background layers based on the game diff
 * It takes background layers prefabs coz size doesnt fit to current screen ratios
 * and setting size dynamically is a mess job
 * once all the layers have fixed size and set for the game, above funtions 
 * can be refactored using loops
 */