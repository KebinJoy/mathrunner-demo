
cc.Class({
    extends: cc.Component,

    properties: {
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.ranges = {
            add: [
                {   // Level 1
                    a: { x: [1, 10], y: [1, 10] },
                    b: { x: [5, 15], y: [5, 15] },
                    c: { x: [16, 25], y: [16, 25] }
                },
                {   // Level 2
                    a: { x: [16, 25], y: [16, 25] },
                    b: { x: [25, 35], y: [25, 35] },
                    c: { x: [35, 45], y: [35, 45] }
                },
                {   // Level 3
                    a: { x: [36, 45], y: [36, 45] },
                    b: { x: [45, 55], y: [45, 55] },
                    c: { x: [55, 65], y: [55, 65] }
                },
                {   // Level 4
                    a: { x: [55, 65], y: [55, 65] },
                    b: { x: [65, 75], y: [65, 75] },
                    c: { x: [75, 99], y: [75, 99] }
                },
                {   // Level 5
                    a: { x: [99, 150], y: [99, 150] },
                    b: { x: [150, 250], y: [150, 250] },
                    c: { x: [250, 350], y: [250, 350] }
                },
                {   // Level 6
                    a: { x: [350, 400], y: [350, 400] },
                    b: { x: [400, 450], y: [400, 450] },
                    c: { x: [450, 499], y: [450, 499] }
                }
            ],
            mul: [
                {   // Level 1
                    a: { x: [1, 10], y: [1, 3] },
                    b: { x: [1, 10], y: [1, 5] },
                    c: { x: [1, 10], y: [1, 7] }
                },
                {   // Level 2
                    a: { x: [2, 10], y: [2, 3] },
                    b: { x: [2, 10], y: [2, 7] },
                    c: { x: [2, 10], y: [3, 8] }
                },
                {   // Level 3
                    a: { x: [3, 10], y: [2, 7] },
                    b: { x: [3, 10], y: [3, 7] },
                    c: { x: [3, 12], y: [4, 10] }
                },
                {   // Level 4
                    a: { x: [4, 12], y: [3, 7] },
                    b: { x: [4, 12], y: [4, 10] },
                    c: { x: [4, 12], y: [5, 12] }
                },
                {   // Level 5
                    a: { x: [5, 12], y: [4, 10] },
                    b: { x: [5, 12], y: [5, 12] },
                    c: { x: [5, 12], y: [6, 14] }
                },
                {   // Level 6
                    a: { x: [6, 14], y: [5, 10] },
                    b: { x: [6, 14], y: [6, 12] },
                    c: { x: [6, 14], y: [7, 14] }
                }
            ],
            sub: [
                {   // Level 1
                    a: { x: [1, 10], y: [1, 10] },
                    b: { x: [5, 15], y: [5, 15] },
                    c: { x: [16, 25], y: [16, 25] }
                },
                {   // Level 2
                    a: { x: [16, 25], y: [16, 25] },
                    b: { x: [25, 35], y: [25, 35] },
                    c: { x: [35, 45], y: [35, 45] }
                },
                {   // Level 3
                    a: { x: [36, 45], y: [36, 45] },
                    b: { x: [45, 55], y: [45, 55] },
                    c: { x: [55, 65], y: [55, 65] }
                },
                {   // Level 4
                    a: { x: [55, 65], y: [55, 65] },
                    b: { x: [65, 75], y: [65, 75] },
                    c: { x: [75, 99], y: [75, 99] }
                },
                {   // Level 5
                    a: { x: [99, 150], y: [99, 150] },
                    b: { x: [150, 250], y: [150, 250] },
                    c: { x: [250, 350], y: [250, 350] }
                },
                {   // Level 6
                    a: { x: [350, 400], y: [350, 400] },
                    b: { x: [400, 450], y: [400, 450] },
                    c: { x: [450, 499], y: [450, 499] }
                }
            ],
            div: [
                {   // Level 1
                    a: { x: [1, 10], y: [1, 3] },
                    b: { x: [1, 10], y: [1, 5] },
                    c: { x: [1, 10], y: [1, 7] }
                },
                {   // Level 2
                    a: { x: [2, 10], y: [2, 3] },
                    b: { x: [2, 10], y: [2, 7] },
                    c: { x: [2, 10], y: [3, 8] }
                },
                {   // Level 3
                    a: { x: [3, 10], y: [2, 7] },
                    b: { x: [3, 10], y: [3, 7] },
                    c: { x: [3, 12], y: [4, 10] }
                },
                {   // Level 4
                    a: { x: [4, 12], y: [3, 7] },
                    b: { x: [4, 12], y: [4, 10] },
                    c: { x: [4, 12], y: [5, 12] }
                },
                {   // Level 5
                    a: { x: [5, 12], y: [4, 10] },
                    b: { x: [5, 12], y: [5, 12] },
                    c: { x: [5, 12], y: [6, 14] }
                },
                {   // Level 6
                    a: { x: [6, 14], y: [5, 10] },
                    b: { x: [6, 14], y: [6, 12] },
                    c: { x: [6, 14], y: [7, 14] }
                }
            ]
        };
        this.qaText = {};
    },

    genSingleOperatorQn(level, difficulty) {
        var reMode ='add';
        var randomMode = Math.floor((Math.random() * 4));
        if (randomMode == 0) reMode = "add";
        else if (randomMode == 1) reMode = "sub";
        else if (randomMode == 2) reMode = "mul";
        else reMode = "div";

        var qaText = "";
        var answer = null;
        var range = this.ranges[reMode][level - 1];
        var getResult = function (x, operator, y) {
            switch (operator) {
                case "add": return x + y;
                case "sub": return x - y;
                case "mul": return x * y;
                case "div": return x / y;
            }
        }
        var getOperatorSign = function (operator) {
            switch (operator) {
                case "add": return ' + ';
                case "sub": return ' - ';
                case "mul": return ' x ';
                case "div": return ' ÷ ';
            }
        }
        var x, y;
        if (reMode == "add" || reMode == "mul") {
            // Addition and multiplication logic
            if (difficulty == 1) {
                x = Math.floor(Math.random() * (range["a"].x[1] - range["a"].x[0] + 1) + range["a"].x[0]);
                y = Math.floor(Math.random() * (range["a"].y[1] - range["a"].y[0] + 1) + range["a"].y[0]);
            } else if (difficulty < 4) {
                x = Math.floor(Math.random() * (range["b"].x[1] - range["b"].x[0] + 1) + range["b"].x[0]);
                y = Math.floor(Math.random() * (range["b"].y[1] - range["b"].y[0] + 1) + range["b"].y[0]);
            } else {
                x = Math.floor(Math.random() * (range["c"].x[1] - range["c"].x[0] + 1) + range["c"].x[0]);
                y = Math.floor(Math.random() * (range["c"].y[1] - range["c"].y[0] + 1) + range["c"].y[0]);
            }
        } else if (reMode == "sub") {
            // Subtraction logic
            if (difficulty == 1) {
                x = Math.floor(Math.random() * (range["a"].x[1] - range["a"].x[0] + 1) + range["a"].x[0]);
                y = Math.floor(Math.random() * (x - range["a"].y[0] + 1) + range["a"].y[0]);
            } else if (difficulty < 4) {
                x = Math.floor(Math.random() * (range["b"].x[1] - range["b"].x[0] + 1) + range["b"].x[0]);
                y = Math.floor(Math.random() * (x - range["b"].y[0] + 1) + range["b"].y[0]);
            } else {
                x = Math.floor(Math.random() * (range["c"].x[1] - range["c"].x[0] + 1) + range["c"].x[0]);
                y = Math.floor(Math.random() * (x - range["c"].y[0] + 1) + range["c"].y[0]);
            }

        } else {
            if (difficulty == 1) {
                x = Math.floor(Math.random() * (range["a"].x[1] - range["a"].x[0] + 1) + range["a"].x[0]);
                y = Math.floor(Math.random() * (range["a"].y[1] - range["a"].y[0] + 1) + range["a"].y[0]);
            } else if (difficulty < 4) {
                x = Math.floor(Math.random() * (range["b"].x[1] - range["b"].x[0] + 1) + range["b"].x[0]);
                y = Math.floor(Math.random() * (range["b"].y[1] - range["b"].y[0] + 1) + range["b"].y[0]);
            } else {
                x = Math.floor(Math.random() * (range["c"].x[1] - range["c"].x[0] + 1) + range["c"].x[0]);
                y = Math.floor(Math.random() * (range["c"].y[1] - range["c"].y[0] + 1) + range["c"].y[0]);
            }
        }
        qaText = x + getOperatorSign(reMode) + y;
        answer = getResult(x, reMode, y);
        this.node.getComponent(cc.Label).string = qaText;
        return { question: qaText, answer: answer.toString() };
    },


    die() {
        this.node.destroy();
    },

    start() {
    },

    // update (dt) {},
});

/**
 * generates qns and sets the string for the label. difficulty got from the result.js
 */