// game difficulty controler and record keeper

cc.Class({
    extends: cc.Component,

    properties: {
        difficulty: {
            default: 1,
            type: cc.Integer
        },
        level : {
            default: 1,
            type: cc.Integer
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.difficulty = this.level = 1;
        this.score = this.total = this.distance = this.stars = this.coins = this.time = this.powerUps = 0;
        this.speed = [100, 120, 140, 160, 180, 200, 210];
    },

    start () {
        this.schedule(function(){
            this.time++;
        },1 , cc.macro.REPEAT_FOREVER);
    },

    // update (dt) {},
});
