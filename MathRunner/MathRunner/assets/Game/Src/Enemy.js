// creates enemy object (not qns).

cc.Class({
    extends: cc.Component,

    properties: {
        hp: {
            default: 20,
            type: 'Integer',
            tooltip: 'Health of Genie',
        },
        longAttack: {
            default: 10,
            type: 'Integer',
            tooltip: 'Long Attack power'
        },
        shortAttack: {
            default: 15,
            type: 'Integer',
            tooltip: 'Short Attack Power',
        },
        longAttackInterval: {
            default: 3,
            type: 'Integer',
            tooltip: 'Interval between long attacks'
        },
        shortAttackInterval: {
            default: 1,
            type: 'Integer',
            tooltip: 'Interval between short attacks'
        },
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        //this.ranges = this.node.getComponent('Questions').ranges;
    },

    start() {
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        manager.enableDebugDraw = true;
    },

  

    update(dt) {
    },

    attack() {

    },

    idle() {

    },
    
    die() {
        //play animation
        //cc.log('Enenmy died');
        this.node.destroy();
    }
});
