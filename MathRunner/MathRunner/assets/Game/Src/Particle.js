
cc.Class({
    extends: cc.Component,

    properties: {
        zIndexVal: {
            default: -1,
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.node.zIndex = this.zIndexVal;
    },

    start () {

    },

    // update (dt) {},
});
