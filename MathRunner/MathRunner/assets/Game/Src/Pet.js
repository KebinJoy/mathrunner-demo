// does nothing.. just pet class and i couldn't extend this to other scripts to avoid repeating properties

var Pet = cc.Class({
    name: 'Pet',
    properties: {
        name: {
            default: 'Pet Draco',
            tooltip: "Pet's Name"
        },
        hp: {
            default: 100,
            type: 'Integer',
            tooltip: 'Health of Genie',
        },
        longAttack: {
            default: 10,
            type: 'Integer',
            tooltip: 'Long Attack power'
        },
        shortAttack: {
            default: 15,
            type: 'Integer',
            tooltip: 'Short Attack Power',
        },
        defence: {
            default: 5,
            type: 'Integer',
            tooltip: 'Defence power'
        },
        defenceInterval: {
            default:10,
            type: 'Integer',
            tooltip: 'Interval toreload defence power'
        },
        defenceSpan: {
            default: 2,
            type: 'Integer',
            tooltip: 'How long a defence move last'
        },
        longAttackInterval: {
            default: 3,
            type: 'Integer',
            tooltip: 'Interval between long attacks'
        },
        shortAttackInterval: {
            default: 1,
            type: 'Integer',
            tooltip: 'Interval between short attacks'
        },
    },

    ctor: function() {
    },
    
});

module.exports = Pet;
cc.Pet = Pet;
