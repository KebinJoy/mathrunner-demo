// pet.js

cc.Class({
    extends: cc.Component,
   
    properties: {
        hp: {
            default: 100,
            type: 'Integer',
            tooltip: 'Health of Genie',
        },
        longAttack: {
            default: 10,
            type: 'Integer',
            tooltip: 'Long Attack power'
        },
        shortAttack: {
            default: 15,
            type: 'Integer',
            tooltip: 'Short Attack Power',
        },
        defence: {
            default: 5,
            type: 'Integer',
            tooltip: 'Defence power'
        },
        defenceInterval: {
            default:10,
            type: 'Integer',
            tooltip: 'Interval toreload defence power'
        },
        defenceSpan: {
            default: 2,
            type: 'Integer',
            tooltip: 'How long a defence move last'
        },
        longAttackInterval: {
            default: 3,
            type: 'Integer',
            tooltip: 'Interval between long attacks'
        },
        shortAttackInterval: {
            default: 1,
            type: 'Integer',
            tooltip: 'Interval between short attacks'
        },
        // uniqueAbility: {
        //     default: 'Defence',
        //     type: cc.String,
        //     tooltip: 'Unique ability <defence, attack, healing>'
        // },
        // uniqueAbilityInterval: {
        //     default: 30,
        //     type: cc.Integer,
        //     tooltip: 'Unique Ability reload interval'
        // },
        // uniqueAbilityPower: {
        //     default: 20,
        //     type: cc.Integer,
        //     tooltip: 'Unique Ability power'
        // },
        // uniqueAbilitySpan: {
        //     default: 6,
        //     type: cc.Integer,
        //     tooltip: 'Unique ability will last for this much time'
        // },
        // element: {
        //     default: 'Fire',
        //     type: cc.String,
        //     tooltip: 'Element of Pet <Air, Water, Fire, Earth, Sky>'
        // },
        weakness: {
            default: 'Division',
            tooltip: 'Weakness of the pet <Addition, Division, Multiplication and Substraction>'
        },
        zIndexVal: {
            default: 9,
            type:cc.Integer,
            tooltip: 'Z index of the node'
        }
    },
    
    onLoad(){
        this.node.zIndex = this.zIndexVal;
    },
    
    start() {
        var manager = cc.director.getCollisionManager();
        manager.enabled = true;
        manager.enableDebugDraw = true;
    },

    onCollissionEnter(){

    },

    update(dt) {
        if (this.hp <= 0){
            this.die();
        }
    },

    idle(){
        //this.spine.setAnimation(0, 'idle', true);
    },

    attack(){
        // let oldAnim = this.spine.animation;
        // this.spine.setAnimation(0,'attack',false);
        // if (oldAnim) {
        //     this.spine.addAnimation(0, oldAnim === 'idle'? 'idle': 'fly', true, 0); //if fly is developed
        // }
    },
    defend(){
        // let oldAnim = this.spine.animation;
        // this.spine.setAnimation(0, 'defence', false);
        // if (oldAnim) {
        //     this.spine.addAnimation(0, oldAnim === 'idle'? 'idle': 'fly', true, o);
        // }
    },
    die() {
        //this.spine.setAnimation(0, 'die', false);
        cc.log('died');
    },

    // moveUp(){
    //     if (this.node.y != 150) {
    //         return this.pet.runAction(cc.moveTo(0.3, cc.v2(-365, 150)).easing(cc.easeCubicActionIn()));
    //     }
    // }
    

});
