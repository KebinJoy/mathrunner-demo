
cc.Class({
    extends: cc.Component,

    properties: {
        petPrefab: {
            default: null,
            type: cc.Prefab,
        },
        enemyPrefab: {
            default: null,
            type: cc.Prefab
        },

        particlePet: {
            default: null,
            type: cc.Prefab
        },
        questionPF: {
            default: null,
            type: cc.Prefab
        }
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.enemyPool = new cc.NodePool();
        let num = 15;
        for (let i=0; i< num; i++){
            let enemy = cc.instantiate(this.enemyPrefab);
            this.enemyPool.put(enemy);
        } //could do same for qns

        this.result = this.node.getComponent('Result');
        this.level = this.result.level;
        this.difficulty = this.result.difficulty;
        this.levelSpeed = this.result.speed[this.level -1];
        this.node.getComponent('Background').speed = this.levelSpeed;
        this.spawnPet();
        this.spawnEnemy();
        this.spawnQuestion();
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    },

    onDestroy() {
        cc.systemEvent.off(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        this.unscheduleAllCallbacks();
    },

    petTakeHit(hitPower) {
        this.pet.hp -= this._pet.defenceNow ? hitPower - this.pet.defence : hitPower;
    },

    start() {
        this._pet.defenceAvail = true;
        this._pet.longAttackAvail = true;
        // this.pet.uniqueAbilityAvail = true;
        this._pet.defenceNow = false;
        // var manager = cc.director.getCollisionManager();
        // manager.enabled = true;
        // manager.enableDebugDraw = true;
        this.answerEntered = '';
    },

    // onCollissionEnter() {

    // },

    spawnPet() {
        this._pet = cc.instantiate(this.petPrefab);
        this.node.addChild(this._pet);
        this._pet.setPosition(-365, 150, 1);
        this._pet.setScale(0.5, 0.5);
        this.pet = this._pet.getComponent('Genie');
    },

    checkAnswer(){ //need to rewrite when there r 2 qns on the screen
        if (this.answerEntered.length == this.qaText.answer.length) {
            if (this.answerEntered == this.qaText.answer) {
                this._pet.y>0? this.petMoveDown(): this.petMoveUp();
                this.deleteEnemy();
                this.deleteQuestion();
                this.spawnEnemy();
                this.spawnQuestion();
            }
            else {
                cc.log(this.answerEntered);
            }
            this.answerEntered = '';
        }
        else if (this.answerEntered.length > this.qaText.answer.length){
            this.answerEntered = '';
        }
    },

    spawnEnemy() {
        this._enemy = this.enemy = null;
        if (this.enemyPool.size()>0){
            this._enemy = this.enemyPool.get();
        }
        else {
            this._enemy = cc.instantiate(this.enemyPrefab);
        }
        this.node.addChild(this._enemy);
        this._enemy.x = 545;
        this._enemy.y = this._pet.y > 0 ? 150 : -150;
        this.enemy = this._enemy.getComponent('Enemy');
    },

    spawnQuestion(){
        this._question = cc.instantiate(this.questionPF);
        this.node.addChild(this._question);
        this._question.setPosition(this._enemy.getPosition());
        this._question.y += 20;
        this.qaText = this._question.getComponent('Questions').genSingleOperatorQn(this.level, this.difficulty);
    },

    deleteQuestion(){
        this._question.getComponent('Questions').die();
    },

    deleteEnemy() {
        this.enemy.die();
    },

    update(dt) {
        this._enemy.x -= this.levelSpeed * dt;
        this._question.x -= this.levelSpeed * dt;
        if (this._enemy.x < -365 || this.enemy.hp <= 0) {
            this.deleteEnemy();
            this.deleteQuestion();
            this.spawnEnemy();
            this.spawnQuestion();
        }
        
    },

    petMoveUp() {
        if (this._pet.y != 150) {
            this._pet.runAction(cc.moveTo(0.3, cc.v2(-365, 150)).easing(cc.easeCubicActionOut()));
            let prtc = cc.instantiate(this.particlePet);
            this.node.addChild(prtc);
            prtc.setRotation(180);
            prtc.setPosition(-365, -150);
            prtc.runAction(cc.moveTo(0.3, cc.v2(-365, 150)).easing(cc.easeCubicActionOut()));
        }
    },

    petMoveDown() {
        if (this._pet.y != -150) {
            this._pet.runAction(cc.moveTo(0.3, cc.v2(-365, -150)).easing(cc.easeCubicActionOut()));
            let prtc = cc.instantiate(this.particlePet);
            this.node.addChild(prtc);
            prtc.setPosition(-365, 150);
            prtc.runAction(cc.moveTo(0.3, cc.v2(-365, -150)).easing(cc.easeCubicActionOut()));
        }
    },

    onKeyDown: function (event) {
        switch (event.keyCode) {
            case cc.macro.KEY.a:
                //attack
                break;

            case cc.macro.KEY.d: //defend
                if (this._pet.defenceAvail) {
                    this._pet.defenceAvail = false;
                    this._pet.defenceNow = true;
                    this.scheduleOnce(function () {
                        this._pet.defenceNow = false;
                    }, this.pet.defenceSpan);
                    this.scheduleOnce(function (dt) {
                        cc.log('time ' + dt + ' defence Avail ' + this.pet.defenceInterval);
                        this._pet.defenceAvail = true;
                    }, this.pet.defenceInterval)
                }
                else {
                    cc.log('defence not available yet');
                }
                break;

            // case cc.macro.KEY.up:
            //     this.petMoveUp();
            //     break;

            // case cc.macro.KEY.down:
            //     this.petMoveDown();
            //     break;

            case cc.macro.KEY.num0:
                this.answerEntered += '0';
                this.checkAnswer();
                break;
            case cc.macro.KEY.num1:
                this.answerEntered += '1';
                this.checkAnswer();
                break;
            case cc.macro.KEY.num2:
                this.answerEntered += '2';
                this.checkAnswer();
                cc.log('2');
                break;
            case cc.macro.KEY.num3:
                this.answerEntered += '3';
                this.checkAnswer();
                cc.log('3');
                break;
            case cc.macro.KEY.num4:
                this.answerEntered += '4';
                this.checkAnswer();
                break;
            case cc.macro.KEY.num5:
                this.answerEntered += '5';
                this.checkAnswer();
                break;
            case cc.macro.KEY.num6:
                this.answerEntered += '6';
                this.checkAnswer();
                break;
            case cc.macro.KEY.num7:
                this.answerEntered += '7';
                this.checkAnswer();
                break;
            case cc.macro.KEY.num8:
                this.answerEntered += '8';
                this.checkAnswer();
                break;
            case cc.macro.KEY.num9:
                this.answerEntered += '9';
                this.checkAnswer();
                break;
            case cc.macro.KEY.space:
                cc.log(this.answerEntered);
                break;
            default:
                cc.log('Case not listed');
        }
    },

    // onKeyUp: function (event) {
    //     switch (event.keyCode) {
    //         case cc.macro.KEY.a:
    //             //attack
    //             break;
    //         case cc.macro.KEY.d:
    //             if (this.defenceAvail) {
    //                 this.defenceNow = true;
    //                 this.scheduleOnce(function () {
    //                     this.defenceNow = false;
    //                 }, this.node.defenceSpan);
    //             }
    //             else {
    //                 cc.log('defence not available untill ' + this.node.defenceSpan + ' secs');
    //             }
    //             break;
    //         case cc.macro.KEY.up:
    //             this.movingUp = true;
    //             break;
    //         case cc.macro.KEY.down:
    //             this.movingDown = true;
    //             break;
    //         default:
    //             cc.log('Case not listed');
    //     }
    // },

});

/**its the game manager and controls other scripts by passing and setting variables
 * could be rewritten and made better coz this one is developed just to learn stuffs
 */